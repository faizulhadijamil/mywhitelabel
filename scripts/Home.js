import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

class HomeScreen extends React.Component {
    
    constructor() {
        super();
        this.state = {
          isLoading: true,
          cars: []
        };
    }

     componentDidMount() {
         
    }

    render() {
       
      return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
           <Text> Home </Text>
        </View>
      );
    }
}

export default HomeScreen;