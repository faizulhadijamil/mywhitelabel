import React from 'react';

import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
// import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import { createBottomTabNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
// // import { createStore, combineReducers } from 'redux';
// // import { connect, Provider } from 'react-redux';

// //import rootReducer from './reducers/NavReducer';

// for pages
import HomeScreen from './Home';
import PromoScreen from './Promopage';
// // import Carscreen from './Carpage';
import Profilescreen from './Profilepage';
import Settingsscreen from './Settingpage';
// // import CarReservationscreen from './CarReservationpage';
// // import CarListscreen from './CarListpage';
// // import CarRegisterscreen from './CarRegisterpage';
// // import CarRegisters2creen from './CarRegister2page';
// // import CarAvailablescreen from './CarAvailablepage';
// // import CarNotAvailablescreen from './CarNotAvailable';

// const CarssStack = createStackNavigator(
//   {
//     Cars: Carscreen,
//     CarRegister: CarRegisterscreen,
//     CarRegister2: CarRegisters2creen,
//     CarList: CarListscreen,
//     CarAvailable: CarAvailablescreen,
//     CarNotAvailable: CarNotAvailablescreen,
//     CarReservation: CarReservationscreen,
//   },
//   {
//     initialRouteName: "Cars",
//   }
// );

const TabNavigator = createBottomTabNavigator(
{
    Home: HomeScreen,
    Promo: PromoScreen,
    // Cars: Carscreen,
    // Cars: CarssStack,
    Profile: Profilescreen,
    Setting: Settingsscreen,
    // Setting: SettingsStack,
},
{
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
            iconName = `home${focused ? '' : ''}`;
        }
        else if (routeName === 'Promo') {
            iconName = `angellist${focused ? '' : ''}`;
        }
        else if (routeName === 'Profile') {
            iconName = `user${focused ? '' : ''}`;
        }
        else if (routeName === 'Setting') {
          iconName = `cog${focused ? '' : ''}`;
        }
        return <IconFontAwesome name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'deepskyblue',
      inactiveTintColor: 'gray',
      style: {
        height: 60,
        paddingVertical: 5,
        backgroundColor: "#fff"
      },
      labelStyle: {
        fontSize: 12,
        lineHeight: 20
      }
    },
  }
);

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });

export default createAppContainer(TabNavigator);

// console.ignoredYellowBox = [
//   'Warning: componentWillMount',
//   'Warning: componentWillReceiveProps',
//   'Warning: componentWillUpdate',
// ];

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

// import React from 'react';
// import {Component} from 'react';
// import {Platform, StyleSheet, Text, View} from 'react-native';

// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
//   android:
//     'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });

// type Props = {};
// export default class App extends Component<Props> {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>Welcome to React Native!</Text>
//         <Text style={styles.instructions}>To get started, edit App.js</Text>
//         <Text style={styles.instructions}>{instructions}</Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });
